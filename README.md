# fireweb_reporter_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## POTENTIAL PERFORMANCE IMPROVEMENTS
- Persist database connection state
- Close database on dispose


## Changing launcher icons
Plugin: https://pub.dev/packages/flutter_launcher_icons

Update the PNG file in icon/icon.png
Run `flutter packages pub run flutter_launcher_icons:main`

## BLOC two-way bind methodology
https://felangel.github.io/bloc/#/
https://github.com/felangel/bloc

## Local secure storage (Authentication tokens)
https://pub.dev/packages/flutter_secure_storage


## Access authentication BLOC
```
final AuthenticationBloc authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
```
Logout example
```
authenticationBloc.dispatch(LoggedOut());
```

## Named route navigation
https://flutter.dev/docs/cookbook/navigation/named-routes

## Local SQL interaction
https://github.com/tekartik/sqflite/blob/master/sqflite/README.md

## Local File Storage interaction
https://flutter.dev/docs/cookbook/persistence/reading-writing-files

## Google Maps and Geolocation
https://medium.com/flutter-io/google-maps-and-flutter-cfb330f9a245 (Read this to set API keys)
https://medium.com/swlh/working-with-geolocation-and-geocoding-in-flutter-and-integration-with-maps-16fb0bc35ede

# Writing custom platform-specific code
https://flutter.dev/docs/development/platform-integration/platform-channels

# DART background tasks
https://flutter.dev/docs/development/packages-and-plugins/background-processes
https://medium.com/flutter-io/executing-dart-in-the-background-with-flutter-plugins-and-geofencing-2b3e40a1a124


# IMPORTANT
Minimum IOS version (For Google Maps to work): 9