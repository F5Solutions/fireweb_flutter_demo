import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/scheduler.dart';

import '_general/loadingIndicator.dart';

import 'instance/instanceBloc.dart';
import 'instance/instanceRepository.dart';
import 'instance/instanceEvents.dart';
import 'instance/instanceState.dart';
import 'instance/instanceModel.dart';

class InstancePage extends StatefulWidget {
  @override
  _InstancePageState createState() => _InstancePageState();
}

class _InstancePageState extends State<InstancePage> {
  InstanceBloc instanceBloc;
  InstanceRepository get instanceRepository => InstanceRepository();

  @override
  void initState() {
    instanceBloc = InstanceBloc(instanceRepository: instanceRepository);
    instanceBloc.dispatch(AppStarted());

    super.initState();
  }

  @override
  void dispose() {
    instanceBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<InstanceBloc>(
      bloc: instanceBloc,
      child: Scaffold(
        body: BlocBuilder<InstanceEvent, InstanceState>(
          bloc: instanceBloc,
          builder: (BuildContext context, InstanceState state) {
            if (state is InstanceSelected) {
              instanceBloc.dispatch(FinishInstance());

              SchedulerBinding.instance.addPostFrameCallback((_) {
                Navigator.pushNamed(context, '/home');
              });
              return LoadingIndicator();
            }

            if (state is InstanceLoading || state is InstanceFinished) {
              return LoadingIndicator();
            }

            if (state is InstanceRequired) {
              instanceBloc.dispatch(GetAccountInstances());
              return LoadingIndicator();
            }

            if (state is InstanceNeedsCapture) {
              final instanceList = state.instanceObject.instanceList;
              String selectedValue = state.api_key;

              final items = instanceList.map((InstanceData data) {
                return new DropdownMenuItem<String>(
                  value: data.api_key,
                  child: new Text(data.name),
                );
              }).toList();

              return MaterialApp(
                theme: new ThemeData(
                  // primarySwatch: Colors.black87,
                  brightness: Brightness.dark,
                  accentColor: Colors.red,
                ),
                home: Scaffold(
                  body: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          _buildLogo(),
                          _buildInstanceSelector(items, selectedValue, state),
                          _buildPadding(),
                          _buildSubmitButton(
                              selectedValue, instanceList, context)
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  SizedBox _buildSubmitButton(String selectedValue,
      List<InstanceData> instanceList, BuildContext context) {
    return SizedBox(
      height: 50.0,
      child: RaisedButton(
        child: SizedBox.expand(
          child: Center(
            child: Text('Save instance'),
          ),
        ),
        onPressed: selectedValue != null
            ? () {
                instanceBloc.dispatch(SaveInstance(
                  instanceList.firstWhere(
                      (instance) => instance.api_key == selectedValue),
                ));
                Navigator.pushNamed(context, '/camera');
              }
            : null,
      ),
    );
  }

  Padding _buildPadding() {
    return Padding(
      padding: EdgeInsets.all(10.0),
    );
  }

  DropdownButtonFormField<String> _buildInstanceSelector(
      List<DropdownMenuItem<String>> items,
      String selectedValue,
      InstanceNeedsCapture state) {
    return DropdownButtonFormField<String>(
      hint: Text('Please select an instance'),
      items: items,
      value: selectedValue,
      onChanged: (_) => {
            instanceBloc.dispatch(SelectedInstance(
              api_key: _,
              instanceObject: state.instanceObject,
            ))
          },
    );
  }

  Image _buildLogo() {
    return Image.asset(
      'assets/logo.png',
    );
  }
}
