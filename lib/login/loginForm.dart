import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'loginBloc.dart';
import 'loginEvents.dart';
import 'loginState.dart';

import '../authentication/authenticationBloc.dart';

class LoginForm extends StatefulWidget {
  final LoginBloc loginBloc;
  final AuthenticationBloc authenticationBloc;

  LoginForm({
    Key key,
    @required this.loginBloc,
    @required this.authenticationBloc,
  }) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  LoginBloc get _loginBloc => widget.loginBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginEvent, LoginState>(
      bloc: _loginBloc,
      builder: (
        BuildContext context,
        LoginState state,
      ) {
        if (state is LoginFailure) {
          _onWidgetDidBuild(() {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          });
        }

        String _validateEmail(String value) {
          Pattern pattern =
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
          RegExp regex = new RegExp(pattern);

          if (!regex.hasMatch(value)) {
            return 'Please enter a valid email address';
          } else {
            return null;
          }
        }

        _buildLogo() {
          return Image.asset(
            'assets/logo.png',
          );
        }

        _buildEmailInput() {
          return TextFormField(
            decoration: InputDecoration(
              labelText: 'Email address',
              border: new OutlineInputBorder(),
            ),
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            validator: _validateEmail,
          );
        }

        _buildPasswordInput() {
          return TextFormField(
            decoration: InputDecoration(
              labelText: 'Password',
              border: new OutlineInputBorder(),
            ),
            controller: _passwordController,
            keyboardType: TextInputType.text,
            obscureText: true,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a password';
              }
            },
          );
        }

        _buildSubmitButton() {
          return SizedBox(
            height: 50.0,
            child: RaisedButton(
              onPressed: state is! LoginLoading ? _onLoginButtonPressed : null,
              child: SizedBox.expand(
                child: Center(
                  child: Text('Login'),
                ),
              ),
            ),
          );
        }

        return Form(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  _buildLogo(),
                  _buildEmailInput(),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                  ),
                  _buildPasswordInput(),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                  ),
                  _buildSubmitButton(),
                  Container(
                    child: state is LoginLoading
                        ? CircularProgressIndicator()
                        : null,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  _onLoginButtonPressed() {
    _loginBloc.dispatch(LoginButtonPressed(
      email: _emailController.text,
      password: _passwordController.text,
    ));
  }
}
