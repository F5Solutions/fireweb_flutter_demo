import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../_general/singleton_api.dart';

class UserRepository {
  final _storage = new FlutterSecureStorage();
  static const String _storageKey_SecurityToken = 'securityToken';

  Future<String> authenticate({
    @required String email,
    @required String password,
  }) async {
    var login = await apiConnection.fetchLogin(email, password);
    return login.access_token;
  }

  Future<void> deleteToken() async {
    await _storage.delete(key: _storageKey_SecurityToken);
    return;
  }

  Future<void> persistToken(String token) async {
    await _storage.write(key: _storageKey_SecurityToken, value: token);
    return;
  }

  Future<bool> hasToken() async {
    String securityToken = await _storage.read(key: _storageKey_SecurityToken);
    if (securityToken == null) {
      return false;
    }
    return securityToken.length > 0;
  }

  Future<String> getToken() async {
    return await _storage.read(key: _storageKey_SecurityToken);
  }
}
