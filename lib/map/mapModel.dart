import 'package:meta/meta.dart';

class MapModel {
  final String latitude;
  final String longitude;
  final String hexColor;

  MapModel({
    @required this.latitude,
    @required this.longitude,
    @required this.hexColor,
  });
}
