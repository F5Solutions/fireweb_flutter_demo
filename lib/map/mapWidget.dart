import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';

import '../_general/loadingIndicator.dart';
import '../_general/hexColor.dart';

import 'mapModel.dart';

class MapWidget extends StatefulWidget {
  final List<MapModel> locations;

  const MapWidget({Key key, this.locations}) : super(key: key);

  @override
  _MapWidgetState createState() => new _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  Completer<GoogleMapController> _completer = Completer();
  GoogleMapController _controller;

  Location location = Location();
  LatLng _center = const LatLng(-33.9319255, 18.4216702);
  final Set<Marker> _markers = {};

  StreamSubscription<LocationData> _positionListener;

  var _buttonIcon = Icons.gps_not_fixed;
  var _markerId = 0;
  var loading = true;
  var trackLocation = false;
  var _cameraMoving = false;

  _onMapCreated(GoogleMapController controller) {
    // _completer.complete(controller);
    _controller = controller;
  }

  _onCameraMove(CameraPosition position) {}

  Future _addAllMarkers(BuildContext context) async {
    final ImageConfiguration imageConfiguration =
        createLocalImageConfiguration(context);

    for (final location in widget.locations) {
      await _addMarker(
        context,
        location.latitude,
        location.longitude,
        imageConfiguration,
        location.hexColor,
      );
    }

    setState(() {
      loading = false;
    });
  }

  static double _getColorFromHex(String hexColor) {
    var color = HexColor(hexColor);
    var hslcolor = HSLColor.fromColor(color);
    var hue = hslcolor.hue;
    return hue;
  }

  Future _addMarker(
    BuildContext context,
    String latitude,
    String longitude,
    ImageConfiguration imageConfiguration,
    String hexColor,
  ) async {
    final _lat = double.parse(latitude);
    final _lng = double.parse(longitude);

    final position = new LatLng(_lat, _lng);

    _markers.add(
      Marker(
          markerId: MarkerId('${++_markerId}'),
          position: position,
          infoWindow: InfoWindow(title: 'Added', snippet: 'Correctly'),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(_getColorFromHex(hexColor))
          // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          // icon: await BitmapDescriptor.fromAssetImage(
          //     imageConfiguration, 'assets/images/mapmarker/marker_blue.png'),
          // icon: BitmapDescriptor.fromAsset('assets/images/mapmarker/marker_blue.png')
          ),
    );
  }

  GoogleMap _buildMap() {
    // The Google™ maps widget
    return GoogleMap(
      onMapCreated: _onMapCreated,
      // onCameraMove: _onCameraMove,
      markers: _markers,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 5.0,
      ),
      compassEnabled: true,
      myLocationButtonEnabled: true,
      rotateGesturesEnabled: false,
      scrollGesturesEnabled: true,
      tiltGesturesEnabled: false,
    );
  }

  _getMapPermissions() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.location]);
      await _getMapPermissions();
    }
  }

  @override
  void initState() {
    _getMapPermissions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      _addAllMarkers(context);
      return LoadingIndicator();
    }

    return Stack(
      children: <Widget>[
        _buildMap(),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
            alignment: Alignment.topRight,
            child: FloatingActionButton(
              onPressed: () async {
                trackLocation = !trackLocation;
                if (!trackLocation) {
                  _positionListener.cancel();
                  setState(() {
                    _buttonIcon = Icons.gps_not_fixed;
                  });
                  return;
                }

                var userLocation = await location.getLocation();
                // await _moveCamera(userLocation);

                _positionListener =
                    location.onLocationChanged().listen((userLocation) {
                  _moveCamera(userLocation);
                });

                setState(() {
                  _buttonIcon = Icons.gps_fixed;
                });
              },
              materialTapTargetSize: MaterialTapTargetSize.padded,
              backgroundColor: Colors.green,
              child: Icon(_buttonIcon, size: 36.0),
            ),
          ),
        ),
      ],
    );
  }

  Future _moveCamera(LocationData userLocation) async {
    if (_cameraMoving) {
      return;
    }
    _cameraMoving = true;
    await _controller.animateCamera(
      CameraUpdate.newLatLngZoom(
          LatLng(userLocation.latitude, userLocation.longitude), 15.0),
    );
    _cameraMoving = false;
  }
}