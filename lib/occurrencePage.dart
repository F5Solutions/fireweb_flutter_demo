import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '_general/loadingIndicator.dart';
import '_general/hexColor.dart';

import 'occurrence/occurrenceBloc.dart';
import 'occurrence/occurrenceRepository.dart';
import 'occurrence/occurrenceEvents.dart';
import 'occurrence/occurrenceState.dart';
import 'occurrence/occurrenceModel.dart';

class OccurrencePage extends StatefulWidget {
  @override
  _OccurrencePageState createState() => _OccurrencePageState();
}

class _OccurrencePageState extends State<OccurrencePage> {
  OccurrenceBloc occurrenceBloc;
  OccurrenceRepository get occurrenceRepository => OccurrenceRepository();

  @override
  void initState() {
    occurrenceBloc = OccurrenceBloc(occurrenceRepository: occurrenceRepository);
    occurrenceBloc.dispatch(AppStarted());

    super.initState();
  }

  @override
  void dispose() {
    occurrenceBloc.dispose();
    super.dispose();
  }

  _buildListView(OccurrenceObject occurrenceObject) {
    return ListView.builder(
      itemCount: occurrenceObject.occurrenceList.length,
      itemBuilder: (context, position) {
        Occurrence occurrence = occurrenceObject.occurrenceList[position];

        // TODO: Get custom color
        // final displayColor = MaterialColor(
        //     HexColor.getColorFromHex(occurrence.type_color),
        //     const <int, Color>{});

        final displayColor = Colors.purple;

        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.info,
                    color: displayColor,
                  ),
                  Text(
                    occurrence.incident_ref_number,
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  Text(occurrence.user_full_name)
                ],
              ),
              Text(occurrence.description)
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OccurrenceBloc>(
      bloc: occurrenceBloc,
      child: Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<OccurrenceEvent, OccurrenceState>(
          bloc: occurrenceBloc,
          builder: (BuildContext context, OccurrenceState state) {
            if (state is OccurrenceLoading) {
              return LoadingIndicator();
            }

            if (state is OccurrenceRecieved) {
              return _buildListView(state.occurrenceObject);
            }
          },
        ),
      ),
    );
  }
}
