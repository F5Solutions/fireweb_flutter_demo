import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../_general/singleton_api.dart';
import '../_general/singleton_database.dart';

import 'reportModel.dart';

class ReportRepository {
  final _storage = new FlutterSecureStorage();

  Future _clearDatabaseTable() async {
    final db = await localDatabase.db;
    await db.delete('Report');
  }

  Future _saveToDatabase(ReportObject reportObject) async {
    await _clearDatabaseTable();
    final db = await localDatabase.db;
    final batch = db.batch();

    for (final Report report in reportObject.reportList) {
      batch.rawInsert(
          'REPLACE INTO Report (ref_number, description, latitude, longitude, occurred_at, type_name, type_code, type_color, type_is_fire_type, type_system_name, reporter_first_name, reporter_last_name, reporter_full_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
          [
            report.ref_number,
            report.description,
            report.latitude,
            report.longitude,
            report.occurred_at,
            report.type_name,
            report.type_code,
            report.type_color,
            report.type_is_fire_type ? 1 : 0,
            report.type_system_name,
            report.reporter_first_name,
            report.reporter_last_name,
            report.reporter_full_name,
          ]);
    }

    await batch.commit(noResult: true);
  }

  Future<ReportObject> _getAllReports() async {
    final db = await localDatabase.db;
    final reports = await db.query('Report');

    return ReportObject.fromDatabaseMap(reports);
  }

  Future<ReportObject> getAll() async {
    final reports = await apiConnection.getActiveReports();
    await _saveToDatabase(reports);
    return reports;
  }
}
