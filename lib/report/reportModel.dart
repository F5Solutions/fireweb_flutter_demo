import '../map/mapModel.dart';

class ReportObject {
  final List<Report> reportList;

  ReportObject({this.reportList});

  factory ReportObject.fromJson(Map<String, dynamic> json) {
    final data = json['data'];
    final List<Report> _reportList = [];

    for (var i = 0; i < data.length; i++) {
      final dataType = data[i]['type'];
      final dataReporter = data[i]['reporter'];

      Report report = Report(
        ref_number: data[i]['ref_number'],
        description: data[i]['description'],
        latitude: data[i]['latitude'],
        longitude: data[i]['longitude'],
        occurred_at: data[i]['occurred_at'],
        type_name: dataType['name'],
        type_code: dataType['code'],
        type_system_name: dataType['system_name'],
        type_color: dataType['color'],
        type_is_fire_type: dataType['is_fire_type'],
        reporter_first_name: dataReporter['first_name'],
        reporter_last_name: dataReporter['last_name'],
        reporter_full_name: dataReporter['full_name'],
      );

      _reportList.add(report);
    }

    return ReportObject(reportList: _reportList);
  }

  factory ReportObject.fromDatabaseMap(List<Map<String, dynamic>> databaseMap) {
    final List<Report> _reportList = [];

    for (Map<String, dynamic> map in databaseMap) {
      Report report = Report(
        ref_number: map['ref_number'],
        description: map['description'],
        latitude: map['latitude'],
        longitude: map['longitude'],
        occurred_at: map['occurred_at'],
        type_name: map['type_name'],
        type_code: map['type_code'],
        type_system_name: map['type_system_name'],
        type_color: map['type_color'],
        type_is_fire_type: map['type_is_fire_type'],
        reporter_first_name: map['reporter_first_name'],
        reporter_last_name: map['reporter_last_name'],
        reporter_full_name: map['reporter_full_name'],
      );

      _reportList.add(report);
    }

    return ReportObject(reportList: _reportList);
  }

  List<MapModel> locations() {
    return reportList
        .map((report) => MapModel(
              latitude: report.latitude,
              longitude: report.longitude,
              hexColor: report.type_color,
            ))
        .toList();
  }
}

class Report {
  final String ref_number;
  final String description;
  final String latitude;
  final String longitude;
  final String occurred_at;

  final String type_name;
  final String type_code;
  final String type_system_name;
  final String type_color;
  final bool type_is_fire_type;

  final String reporter_first_name;
  final String reporter_last_name;
  final String reporter_full_name;

  Report({
    this.ref_number,
    this.description,
    this.latitude,
    this.longitude,
    this.occurred_at,
    this.type_name,
    this.type_code,
    this.type_system_name,
    this.type_color,
    this.type_is_fire_type,
    this.reporter_first_name,
    this.reporter_last_name,
    this.reporter_full_name,
  });
}
