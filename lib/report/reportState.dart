import 'package:equatable/equatable.dart';
import 'reportModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ReportState extends Equatable {
  ReportState([List props = const []]) : super(props);
}

class ReportLoading extends ReportState {
  @override
  String toString() => 'ReportLoading';
}

class ReportRecieved extends ReportState {
  final ReportObject reportObject;

  ReportRecieved([this.reportObject]) : super([reportObject]);

  @override
  String toString() => 'ReportRecieved';
}

class ReportSelected extends ReportState {
  @override
  String toString() => 'ReportSelected';
}
