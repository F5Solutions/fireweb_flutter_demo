import 'package:equatable/equatable.dart';
import 'reportModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ReportEvent extends Equatable {
  ReportEvent([List props = const []]) : super(props);
}

class AppStarted extends ReportEvent {
  
  @override
  String toString() => 'AppStarted';
}

class GetReports extends ReportEvent {
  @override
  String toString() => 'GetReports';
}