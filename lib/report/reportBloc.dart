import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'reportEvents.dart';
import 'reportState.dart';
import 'reportRepository.dart';

class ReportBloc extends Bloc<ReportEvent, ReportState> {
  final ReportRepository reportRepository;

  ReportBloc({@required this.reportRepository});

  @override
  ReportState get initialState => ReportLoading();

  @override
  Stream<ReportState> mapEventToState(ReportEvent event) async* {
    if(event is AppStarted) {
      yield ReportLoading();
      final reports = await reportRepository.getAll();
      yield ReportRecieved(reports);
    }
  }
}