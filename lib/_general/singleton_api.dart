import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'dart:io';

import 'dart:async';

import 'singleton_const.dart';

import '../login/loginModel.dart';
import '../instance/instanceModel.dart';
import '../report/reportModel.dart';
import '../occurrence/occurrenceModel.dart';

final ApiConnection apiConnection = new ApiConnection._private();

class ApiConnection {
  ApiConnection._private();

  final _storage = new FlutterSecureStorage();

  bool _hasValidEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);

    return regex.hasMatch(value);
  }

  Future<String> _getSecurityToken() async {
    return await _storage.read(key: consts.storageKey_SecurityToken);
  }

  Future<String> _getApiKey() async {
    return await _storage.read(key: consts.storageKey_InstanceApiKey);
  }

  Future<LoginModel> fetchLogin(String email, String password) async {
    if (email.isEmpty || password.isEmpty) {
      throw Exception('Please enter an email address and password');
    }

    if (!_hasValidEmail(email)) {
      throw Exception('Invalid email address');
    }

    final response = await http.post(
      '${consts.apiBaseUrl}/oauth/token',
      body: {
        "grant_type": "password",
        "client_id": "2",
        "client_secret": "6rX4nI4o1937Vxe7AagwyYBJAQ7MF3r1HpRP2ZCA",
        "username": email,
        "password": password.toString(),
        "scope": "*"
      },
    );

    if (response.statusCode == 200) {
      return LoginModel.fromJson(json.decode(response.body));
    }
    if (response.statusCode == 401) {
      throw Exception('Invalid username or password');
    } else {
      throw Exception('Unable to log you in');
    }
  }

  Future<InstanceObject> getInstancesForAccount() async {
    final token = await _getSecurityToken();

    final response = await http.get(
      '${consts.apiBaseUrl}/api/instances',
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        HttpHeaders.acceptHeader: 'application/json'
      },
    );

    final decodedJson = json.decode(response.body);
    final responseBody = InstanceObject.fromJson(decodedJson);

    return responseBody;
  }

  Future<ReportObject> getActiveReports() async {
    final apiKey = await _getApiKey();
    final token = await _getSecurityToken();

    final response = await http.get(
      '${consts.apiBaseUrl}/api/reports?api_key=$apiKey&status=active',
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        HttpHeaders.acceptHeader: 'application/json'
      },
    );

    final decodedJson = json.decode(response.body);
    final responseBody = ReportObject.fromJson(decodedJson);

    return responseBody;
  }

  Future<ReportObject> getAllReports() async {
    final apiKey = await _getApiKey();
    final token = await _getSecurityToken();

    final response = await http.get(
      '${consts.apiBaseUrl}/api/reports?api_key=$apiKey',
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        HttpHeaders.acceptHeader: 'application/json'
      },
    );

    final decodedJson = json.decode(response.body);
    final responseBody = ReportObject.fromJson(decodedJson);

    return responseBody;
  }

  Future<OccurrenceObject> getActiveOccurrences() async {
    final apiKey = await _getApiKey();
    final token = await _getSecurityToken();

    final response = await http.get(
      '${consts.apiBaseUrl}/api/occurrences?api_key=$apiKey',
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        HttpHeaders.acceptHeader: 'application/json'
      },
    );

    final decodedJson = json.decode(response.body);
    final responseBody = OccurrenceObject.fromJson(decodedJson);

    return responseBody;
  }
}
