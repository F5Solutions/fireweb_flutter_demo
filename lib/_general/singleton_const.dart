final Const consts = new Const._private();

class Const {
  Const._private();

  final String apiBaseUrl = 'http://app-staging-fireweb.syw.io';
  final String storageKey_SecurityToken = 'securityToken';
  final String storageKey_InstanceApiKey = 'hasInstances';
  final String databaseName = 'fireweb_app.db';
}
