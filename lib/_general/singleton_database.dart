import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'singleton_const.dart';

final LocalDatabase localDatabase = new LocalDatabase._private();

class LocalDatabase {
  LocalDatabase._private();

  static Database _database;

  static const String _occurranceCreateTable =
      'CREATE TABLE IF NOT EXISTS Occurrence (incident_ref_number TEXT PRIMARY KEY, description TEXT, actioned_at TEXT, user_first_name TEXT, user_last_name TEXT, user_full_name TEXT)';
  static const String _instanceCreateTable =
      'CREATE TABLE Instance (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, api_key TEXT)';
  static const String _instanceTypeCreateTable =
      'CREATE TABLE InstanceType (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, code TEXT, system_name TEXT)';
  static const String _reportCreateTable =
      'CREATE TABLE IF NOT EXISTS Report (ref_number TEXT PRIMARY KEY, description TEXT, latitude TEXT, longitude TEXT, occurred_at TEXT, type_name TEXT, type_code TEXT, type_color TEXT, type_is_fire_type INTEGER, type_system_name TEXT, reporter_first_name TEXT, reporter_last_name TEXT, reporter_full_name TEXT)';

  Future<Database> get db async {
    if (_database != null) {
      return _database;
    }

    await _init();
    return _database;
  }

  Future _init() async {
    final databasesPath = await getDatabasesPath();
    String path = join(databasesPath, consts.databaseName);

    _database = await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  FutureOr<void> _onCreate(Database db, int version) async {
    await db.execute(_occurranceCreateTable);
    await db.execute(_instanceCreateTable);
    await db.execute(_instanceTypeCreateTable);
    await db.execute(_reportCreateTable);
  }
}
