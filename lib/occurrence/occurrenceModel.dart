import '../map/mapModel.dart';

class OccurrenceObject {
  final List<Occurrence> occurrenceList;

  OccurrenceObject({this.occurrenceList});

  factory OccurrenceObject.fromJson(Map<String, dynamic> json) {
    final data = json['data'];
    final List<Occurrence> _occurrenceList = [];

    for (var i = 0; i < data.length; i++) {
      final dataUser = data[i]['user'];

      Occurrence occurrence = Occurrence(
        incident_ref_number: data[i]['incident_ref_number'],
        description: data[i]['description'],
        actioned_at: data[i]['actioned_at'],
        user_first_name: dataUser['first_name'],
        user_last_name: dataUser['last_name'],
        user_full_name: dataUser['full_name'],
      );

      _occurrenceList.add(occurrence);
    }

    return OccurrenceObject(occurrenceList: _occurrenceList);
  }

  factory OccurrenceObject.fromDatabaseMap(
      List<Map<String, dynamic>> databaseMap) {
    final List<Occurrence> _occurrenceList = [];

    for (Map<String, dynamic> map in databaseMap) {
      Occurrence occurrence = Occurrence(
        incident_ref_number: map['incident_ref_number'],
        description: map['description'],
        actioned_at: map['actioned_at'],
        user_first_name: map['user_first_name'],
        user_last_name: map['user_last_name'],
        user_full_name: map['user_full_name'],
      );

      _occurrenceList.add(occurrence);
    }

    return OccurrenceObject(occurrenceList: _occurrenceList);
  }
}

class Occurrence {
  final String incident_ref_number;
  final String actioned_at;
  final String description;

  final String user_first_name;
  final String user_last_name;
  final String user_full_name;

  Occurrence({
    this.incident_ref_number,
    this.actioned_at,
    this.description,
    this.user_first_name,
    this.user_last_name,
    this.user_full_name,
  });
}
