import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../_general/singleton_api.dart';
import '../_general/singleton_database.dart';

import 'occurrenceModel.dart';

class OccurrenceRepository {
  final _storage = new FlutterSecureStorage();
  Future _clearDatabaseTable() async {
    final db = await localDatabase.db;
    await db.delete('Occurrence');
  }

  Future _saveToDatabase(OccurrenceObject occurrenceObject) async {
    await _clearDatabaseTable();
    final db = await localDatabase.db;
    final batch = db.batch();

    for (final Occurrence occurrence in occurrenceObject.occurrenceList) {
      batch.rawInsert(
          'REPLACE INTO Occurrence (incident_ref_number, description, actioned_at, user_first_name, user_last_name, user_full_name) VALUES (?, ?, ?, ?, ?, ?)',
          [
            occurrence.incident_ref_number,
            occurrence.description,
            occurrence.actioned_at,
            occurrence.user_first_name,
            occurrence.user_last_name,
            occurrence.user_full_name
          ]);
    }

    await batch.commit(noResult: true);
  }

  Future<OccurrenceObject> _getAllOccurrences() async {
    final db = await localDatabase.db;
    final occurrences = await db.query('Occurrence');

    return OccurrenceObject.fromDatabaseMap(occurrences);
  }

  Future<OccurrenceObject> getAll() async {
    final occurrences = await apiConnection.getActiveOccurrences();
    await _saveToDatabase(occurrences);
    return occurrences;
  }
}
