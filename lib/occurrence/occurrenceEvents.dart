import 'package:equatable/equatable.dart';
import 'occurrenceModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class OccurrenceEvent extends Equatable {
  OccurrenceEvent([List props = const []]) : super(props);
}

class AppStarted extends OccurrenceEvent {
  
  @override
  String toString() => 'AppStarted';
}

class GetOccurrences extends OccurrenceEvent {
  @override
  String toString() => 'GetOccurrences';
}