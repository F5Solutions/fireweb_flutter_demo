import 'package:equatable/equatable.dart';
import 'occurrenceModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class OccurrenceState extends Equatable {
  OccurrenceState([List props = const []]) : super(props);
}

class OccurrenceLoading extends OccurrenceState {
  @override
  String toString() => 'OccurrenceLoading';
}

class OccurrenceRecieved extends OccurrenceState {
  final OccurrenceObject occurrenceObject;

  OccurrenceRecieved([this.occurrenceObject]) : super([occurrenceObject]);

  @override
  String toString() => 'OccurrenceRecieved';
}

class OccurrenceSelected extends OccurrenceState {
  @override
  String toString() => 'OccurrenceSelected';
}
