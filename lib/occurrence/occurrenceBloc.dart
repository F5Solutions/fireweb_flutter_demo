import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'occurrenceEvents.dart';
import 'occurrenceState.dart';
import 'occurrenceRepository.dart';

class OccurrenceBloc extends Bloc<OccurrenceEvent, OccurrenceState> {
  final OccurrenceRepository occurrenceRepository;

  OccurrenceBloc({@required this.occurrenceRepository});

  @override
  OccurrenceState get initialState => OccurrenceLoading();

  @override
  Stream<OccurrenceState> mapEventToState(OccurrenceEvent event) async* {
    if(event is AppStarted) {
      yield OccurrenceLoading();
      final occurrences = await occurrenceRepository.getAll();
      yield OccurrenceRecieved(occurrences);
    }
  }
}