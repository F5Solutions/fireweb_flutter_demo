class CreateFields {
  final String uuid;
  final String incidentType;
  final String fireDangerIndex;
  final String locationLatitude;
  final String locationLongitude;
  final String incidentDate;
  final String incidentTime;
  final String incidentDetails;
  final String incidentFiles; // Pipe delimetered string (loc/1.png|loc/2.png)

  CreateFields(
      this.uuid,
      this.incidentType,
      this.fireDangerIndex,
      this.locationLatitude,
      this.locationLongitude,
      this.incidentDate,
      this.incidentTime,
      this.incidentDetails,
      this.incidentFiles);
}
