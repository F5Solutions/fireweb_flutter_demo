import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:uuid/uuid.dart';
import 'package:sqflite/sqflite.dart';

import '../_general/singleton_const.dart';

import '_cameraView.dart';

enum PageMarker { type, dangerIndex, loation, details, images }

class CreatePage extends StatelessWidget {
  String _currentUuid; // For local reference
  int _currentDatabaseId; // For database reference

  Database _database;

  void _openDatabase() async {
    final databasesPath = await getDatabasesPath();
    String path = join(databasesPath, consts.databaseName);

    // TODO: Delete this on production
    await deleteDatabase(path);

    _database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute('''
          CREATE TABLE WaitingIncidents 
            (
              id INTEGER PRIMARY KEY, 
              uuid TEXT, 
              incidentType TEXT, 
              fireDangerIndex TEXT, 
              locationLatitude TEXT, 
              locationLongitude TEXT, 
              incidentDate TEXT, 
              incidentTime TEXT, 
              incidentDetails TEXT, 
              incidentFiles TEXT, 
              uuid TEXT, 
              uuid TEXT, 
              uuid TEXT, 
              uuid TEXT,
            )
          ''');
    });
  }

  void _saveToDatabase() async {
    await _database.transaction((txn) async {
      _currentDatabaseId = await txn
          .rawInsert('INSERT INTO WaitingIncidents VALUES ($_currentUuid)');
    });
  }

  @override
  void initState() {
    final uuid = Uuid();
    _currentUuid = uuid.v4();

    _openDatabase();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(38, 43, 47, 1),
        title: Text("Add Incident"),
      ),
      body: CameraView(),
    );
  }
}
