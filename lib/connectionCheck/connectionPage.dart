import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ConnectionRoute extends StatefulWidget {
  @override
  _ConnectionState createState() => _ConnectionState();
}

class _ConnectionState extends State<ConnectionRoute> {
  final client = new http.Client();
  static const _url = 'http://app-staging-fireweb.syw.io/api';

  Future<PingResponse> _getPing() async {
    const url = '$_url/ping';
    final response = await client.get(url);

    if (response.statusCode == 200) {
      return PingResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to check ping');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Connection Test'),
      ),
      body: Center(
        child: FutureBuilder<PingResponse>(
          future: _getPing(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Text(snapshot.data.message);
            } else if (snapshot.hasError) {
              return Text(snapshot.error);
            }

            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    client.close();
    super.dispose();
  }
}

class PingResponse {
  final String message;

  PingResponse({this.message});

  factory PingResponse.fromJson(Map<String, dynamic> json) {
    return PingResponse(
      message: json['message'],
    );
  }
}
