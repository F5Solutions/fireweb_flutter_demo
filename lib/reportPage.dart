import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'map/mapWidget.dart';

import '_general/loadingIndicator.dart';
import '_general/hexColor.dart';

import 'report/reportBloc.dart';
import 'report/reportRepository.dart';
import 'report/reportEvents.dart';
import 'report/reportState.dart';
import 'report/reportModel.dart';

class ReportPage extends StatefulWidget {
  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  ReportBloc reportBloc;
  ReportRepository get reportRepository => ReportRepository();

  @override
  void initState() {
    reportBloc = ReportBloc(reportRepository: reportRepository);
    reportBloc.dispatch(AppStarted());

    super.initState();
  }

  @override
  void dispose() {
    reportBloc.dispose();
    super.dispose();
  }

  _buildListView(ReportObject reportObject) {
    return ListView.builder(
      itemCount: reportObject.reportList.length,
      itemBuilder: (context, position) {
        Report report = reportObject.reportList[position];

        final displayColor = MaterialColor(
            HexColor.getColorFromHex(report.type_color), const <int, Color>{});

        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _buildTitle(displayColor, report),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: displayColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(report.reporter_full_name),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.calendar_today,
                          color: displayColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(report.occurred_at),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 10),
                      child: Text(report.description),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Container _buildTitle(MaterialColor displayColor, Report report) {
    return Container(
      decoration: BoxDecoration(
        color: displayColor,
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              report.ref_number,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            Icon(
              Icons.info,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ReportBloc>(
      bloc: reportBloc,
      child: Scaffold(
        body: BlocBuilder<ReportEvent, ReportState>(
          bloc: reportBloc,
          builder: (BuildContext context, ReportState state) {
            if (state is ReportLoading) {
              return LoadingIndicator();
            }

            if (state is ReportRecieved) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: MapWidget(
                      locations: state.reportObject.locations(),
                    ),
                  ),
                  Expanded(
                    child: _buildListView(state.reportObject),
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
