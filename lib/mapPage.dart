import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapRoute extends StatefulWidget {
  @override
  State<MapRoute> createState() => MapState();
}

class MapState extends State<MapRoute> {
  final Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();

  static const bool gotLocation = false;

  LocationData startLocation;
  LocationData currentLocation;
  bool _hasPermission = false;
  bool _serviceEnabled = false;
  bool _currentlyActive = false;
  var error;
  StreamSubscription<LocationData> locationSubscription;
  CameraPosition _currentCameraPosition;

  GoogleMap googleMap;

  var _location = new Location();

  static final LatLng _center = const LatLng(-33.9319255, 18.4216702);
  LatLng _lastMapPosition = _center;

  void _onMapCreated(GoogleMapController controller) {
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    }
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: '- WARNING -',
          snippet: 'Potential fire alert',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  // TODO: Make this work :)
  void _getCurrentLocation() async {
    if (_currentlyActive) {
      _currentlyActive = false;
      locationSubscription.cancel();
      return;
    }
    _currentlyActive = true;

    // gotLocation = true;
    LocationData location;

    print('Getting current location');

    await _location.changeSettings(
        accuracy: LocationAccuracy.HIGH, interval: 1000);

    try {
      _serviceEnabled = await _location.serviceEnabled();

      if (_serviceEnabled) {
        print('Service is enabled');
        _hasPermission = await _location.requestPermission();

        print('Has permissions: $_hasPermission');

        if (_hasPermission) {
          print('Permissions accepted');
          location = await _location.getLocation();

          locationSubscription =
              _location.onLocationChanged().listen((LocationData result) async {
            print('Location updated');
            _currentCameraPosition = CameraPosition(
                target: LatLng(result.latitude, result.longitude), zoom: 15);

            final GoogleMapController controller = await _controller.future;
            controller.animateCamera(
                CameraUpdate.newCameraPosition(_currentCameraPosition));

            setState(() {
              currentLocation = result;
            });
          });
        }
      } else {
        print('Service not enabled, requesting');
        bool serviceResult = await _location.requestService();
        if (serviceResult) {
          _getCurrentLocation();
        }
      }
    } on PlatformException catch (e) {
      location = null;
      error = e;
    }

    setState(() {
      startLocation = location;
    });
  }

  @override
  Widget build(BuildContext context) {
    // The Google™ maps widget
    googleMap = GoogleMap(
      onMapCreated: _onMapCreated,
      onCameraMove: _onCameraMove,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 15.0,
      ),
      markers: _markers,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Maps Sample App'),
      ),
      body: Stack(
        children: <Widget>[
          googleMap,
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    onPressed: _getCurrentLocation,
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    child: const Icon(
                        gotLocation ? Icons.gps_fixed : Icons.gps_not_fixed,
                        size: 36.0),
                    heroTag: 'btn1',
                  ),
                  SizedBox(height: 16.0),
                  FloatingActionButton(
                    onPressed: _onAddMarkerButtonPressed,
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.green,
                    child: const Icon(Icons.add_location, size: 36.0),
                    heroTag: 'btn2',
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
