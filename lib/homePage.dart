import 'package:flutter/material.dart';

import 'reportPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(38, 43, 47, 1),
        leading: FlatButton(
          onPressed: () {
            Navigator.pushNamed(context, '/camera');
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pushNamed(context, '/occurrence');
            },
            child: Icon(
              Icons.menu,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: ReportPage(),
    );
  }
}
