import 'package:equatable/equatable.dart';
import 'instanceModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class InstanceState extends Equatable {
  InstanceState([List props = const []]) : super(props);
}

class InstanceSelected extends InstanceState {
  final String api_key;

  InstanceSelected([this.api_key]) : super([api_key]);

  @override
  String toString() => 'InstanceSelected';
}

class InstanceRequired extends InstanceState {
  @override
  String toString() => 'InstanceRequired';
}

class InstanceNeedsCapture extends InstanceState {
  final InstanceObject instanceObject;
  final String api_key;

  InstanceNeedsCapture([this.instanceObject, this.api_key]) : super([instanceObject, api_key]);

  @override
  String toString() => 'InstanceNeedsCapture';
}

class InstanceLoading extends InstanceState {
  @override
  String toString() => 'InstanceLoading';
}

class InstanceFailed extends InstanceState {
  @override
  String toString() => 'InstanceFailed';
}

class InstanceFinished extends InstanceState {
  @override
  String toString() => 'InstanceFinished';
}