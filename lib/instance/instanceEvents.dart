import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'instanceModel.dart';

@immutable
abstract class InstanceEvent extends Equatable {
  InstanceEvent([List props = const []]) : super(props);
}

class AppStarted extends InstanceEvent {
  @override
  String toString() => 'AppStarted';
}

class GetAccountInstances extends InstanceEvent {
  @override
  String toString() => 'GetAccountInstances';
}

class SaveInstance extends InstanceEvent {
  final InstanceData instanceData;

  SaveInstance(this.instanceData) : super([instanceData]);

  @override
  String toString() => 'SaveInstance';
}

class GetInstance extends InstanceEvent {
  @override
  String toString() => 'GetInstance';
}

class SelectedInstance extends InstanceEvent {
  final InstanceObject instanceObject;
  final String api_key;

  SelectedInstance({
    @required this.api_key,
    this.instanceObject
  }) : super([api_key]);

  @override
  String toString() =>
      'InstanceSelected { api_key: $api_key }';
}

class FinishInstance extends InstanceEvent {
  @override
  String toString() => 'FinishInstance';
}