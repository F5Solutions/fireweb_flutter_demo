import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'instanceEvents.dart';
import 'instanceState.dart';
import 'instanceRepository.dart';

class InstanceBloc extends Bloc<InstanceEvent, InstanceState> {
  final InstanceRepository instanceRepository;

  InstanceBloc({@required this.instanceRepository});

  @override
  InstanceState get initialState => InstanceLoading();

  @override
  Stream<InstanceState> mapEventToState(InstanceEvent event) async* {
    if(event is FinishInstance) {
      yield InstanceFinished();
      return;
    }

    if (event is AppStarted) {
      final instance = await instanceRepository.getInstance();

      if (instance != null) {
        yield InstanceSelected(instance);
      } else {
        yield InstanceRequired();
      }
    }

    if (event is GetAccountInstances) {
      yield InstanceLoading();
      final instances = await instanceRepository.getInstancesForAccount();
      yield InstanceNeedsCapture(instances);
    }

    if (event is SaveInstance) {
      yield InstanceLoading();
      final instance =
          await instanceRepository.saveInstance(event.instanceData);
      yield InstanceSelected(instance);
    }

    if (event is GetInstance) {
      yield InstanceLoading();
      final instance = await instanceRepository.getInstance();
      yield InstanceSelected(instance);
    }

    if (event is SelectedInstance) {
      yield InstanceNeedsCapture(event.instanceObject, event.api_key);
    }
  }
}
