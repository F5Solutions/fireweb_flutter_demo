class InstanceObject {
  final List<InstanceData> instanceList;
  final InstanceData selectedInstance;

  InstanceObject({this.instanceList, this.selectedInstance});

  factory InstanceObject.fromJson(Map<String, dynamic> json) {
    final data = json['data'];
    final List<InstanceData> _instanceList = [];

    for (var i = 0; i < data.length; i++) {
      List<InstanceType> listTypes = [];

      final types = data[i]['types'];

      for (var ti = 0; ti < types.length; ti++) {
        listTypes.add(InstanceType(
          types[i]['name'],
          types[i]['code'],
          types[i]['system_name'],
        ));
      }

      _instanceList.add(InstanceData(
        data[i]['name'],
        data[i]['api_key'],
        listTypes,
      ));
    }

    return InstanceObject(instanceList: _instanceList);
  }
}

class InstanceData {
  final String name;
  final String api_key;
  final List<InstanceType> types;

  InstanceData(this.name, this.api_key, this.types);
}

class InstanceType {
  final String name;
  final String code;
  final String system_name;

  InstanceType(this.name, this.code, this.system_name);
}

enum InstanceFilter { all, active }
