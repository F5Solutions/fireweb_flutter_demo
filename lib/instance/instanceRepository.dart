import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../_general/singleton_api.dart';
import '../_general/singleton_const.dart';
import '../_general/singleton_database.dart';

import 'instanceModel.dart';

class InstanceRepository {
  final _storage = new FlutterSecureStorage();

  Future _saveToDatabase(InstanceData instanceData) async {
    final db = await localDatabase.db;
    final batch = db.batch();
    batch.insert('Instance',
        {'name': instanceData.name, 'api_key': instanceData.api_key});

    instanceData.types.forEach((instanceType) => {
          batch.insert('InstanceType', {
            'name': instanceType.name,
            'code': instanceType.code,
            'system_name': instanceType.system_name
          })
        });

    await batch.commit(noResult: true);
  }

  Future persistInstanceApiKey(String api_key) async {
    await _storage.write(key: consts.storageKey_InstanceApiKey, value: api_key);
  }

  Future<bool> hasInstances() async {
    String hasInstances = await _storage.read(key: consts.storageKey_InstanceApiKey);
    return (hasInstances == null || hasInstances == 'false') ? false : true;
  }

  Future<String> getInstanceFromStorage() async {
    return await _storage.read(key: consts.storageKey_InstanceApiKey);
  }

  Future<InstanceObject> getInstancesForAccount() async {
    return await apiConnection.getInstancesForAccount();
  }

  Future<String> saveInstance(InstanceData instance) async {
    await _saveToDatabase(instance);
    await persistInstanceApiKey(instance.api_key);

    return instance.api_key;
  }

  Future<String> getInstance() async {
    final hasInstance = await hasInstances();
    if (!hasInstance) {
      return null;
    }
    final instance = await getInstanceFromStorage();
    return instance;
  }
}
