import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';

import 'authentication/userRepository.dart';
import 'authentication/authenticationBloc.dart';
import 'authentication/authenticationEvents.dart';
import 'authentication/authenticationState.dart';

import '_general/loadingIndicator.dart';
import 'loginPage.dart';
import 'instancePage.dart';
import 'homePage.dart';
import 'occurrencePage.dart';
import 'splashPage.dart';

import 'connectionCheck/connectionPage.dart';

import 'cameraPage.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
  }
}

void main() {
  BlocSupervisor().delegate = SimpleBlocDelegate();

  runApp(App(
    userRepository: UserRepository(),
  ));
}

class App extends StatefulWidget {
  final UserRepository userRepository;

  App({Key key, @required this.userRepository}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  AuthenticationBloc authenticationBloc;
  UserRepository get userRepository => widget.userRepository;

  @override
  void initState() {
    authenticationBloc = AuthenticationBloc(userRepository: userRepository);
    authenticationBloc.dispatch(AppStarted());
    super.initState();
  }

  @override
  void dispose() {
    authenticationBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      bloc: authenticationBloc,
      child: MaterialApp(
        home: BlocBuilder<AuthenticationEvent, AuthenticationState>(
          bloc: authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state is AuthenticationUninitialized) {
              return SplashPage();
            }

            if (state is AuthenticationAuthenticated) {
              return MaterialApp(
                theme: new ThemeData(
                  primarySwatch: Colors.blueGrey,
                  accentColor: Colors.red,
                ),
                initialRoute: '/',
                onGenerateRoute: (settings) {
                  switch (settings.name) {
                    case '/':
                      return PageTransition(
                        child: InstancePage(),
                        type: PageTransitionType.fade,
                      );
                      break;
                    case '/home':
                      return PageTransition(
                        child: HomePage(),
                        type: PageTransitionType.fade,
                      );
                      break;
                    case '/occurrence':
                      return PageTransition(
                        child: OccurrencePage(),
                        type: PageTransitionType.rightToLeftWithFade,
                      );
                      break;
                    case '/camera':
                      return PageTransition(
                        child: CameraRoute(),
                        type: PageTransitionType.leftToRightWithFade,
                      );
                      break;
                  }
                },
                // routes: {
                //   '/': (context) => InstancePage(),
                //   '/home': (context) => HomePage(),
                //   // '/create': (context) => CreatePage(),
                //   '/occurrence': (context) => OccurrencePage(),
                //   '/connect': (context) => ConnectionRoute(),
                //   '/camera': (context) => CameraRoute()
                // },
              );
            }

            if (state is AuthenticationUnauthenticated) {
              return LoginPage(userRepository: userRepository);
            }

            if (state is AuthenticationLoading) {
              return LoadingIndicator();
            }
          },
        ),
      ),
    );
  }
}
